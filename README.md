# Introduction aux systèmes de cartographie

![scrat](img/scrat-earth.jpg)

Le paysage des systèmes d'information géographique (SIG), aussi dénommés GIS en
anglais (Geographic Information System) est très vaste.

On peut citer quelques exemples d'acteurs privés:
[Google Map](https://www.google.com/maps), [MapBox](https://www.mapbox.com/),
[BING map](https://www.bing.com/maps/), [ESRI](https://www.esri.com).

Les états et regroupements d'états sont aussi très présents dans ce domaine de
par son caractère régalien (c'est à dire qui a trait à la souveraineté).
On peut citer par exemple: les agences spaciales des différents continents et
états: [ESA](http://www.esa.int/ESA), [NASA](https://www.nasa.gov/),
[ROSCOSMOS](http://en.roscosmos.ru), [CNES](https://cnes.fr/fr/).

[L'ouverture des données publiques](https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es_ouverte)
est aussi une source très importante pour l'ensemble des acteurs.

Des solutions génériques de cartographie existent aussi. Elles sont en général
mises à disposition sous forme de
[logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre) ou via des
services en ligne.
On peut citer par exemple: [Openstreetmap](https://www.openstreetmap.org) et
[OpenLayers](http://openlayers.org/).


# Thème de ce document

Nous nous intéresserons dans ce document à la solution en logiciel libre
[OpenLayers](http://openlayers.org/).
Nous manipulerons cette solution via le langage javascript dans un navigateur
web (partie "front-end").

Une partie "back-end" complètera ce dispositif pour alimenter en données et
effectuer des calculs. Cette partie "back-end" pourra être effectuées aussi
en javascript (via nodejs) ou bien dans le langage PHP.

Le principe [REST](https://en.wikipedia.org/wiki/Representational_state_transfer)
sera utilisé entre le "front-end" et le "back-end" via le format JSON.

Voici ci-dessous une liste non-exhaustive d'avantages et de faiblesses liées
au choix de la solution OpenLayers.

Avantages:
--------

- pérénnité: l'ensemble des cartes et logiciels peuvent être téléchargés et
utilisés indépendamment. Cela permet par exemple de ne pas être affecté par un
changement d'API ou de changement des conditions d'utilisation.

- logiciel libre: l'ensemble du code source est disponible et modifiable par
quiconque du moment qu'ils les rend disponible à la communauté.

- gratuité: le prix des solutions commerciales, souvent proportionnel au
nombre de consultations peut affecter un pourcentage important du chiffre
d'affaire d'une entreprise.

- utilisation de formats de données [interopérables](https://fr.wikipedia.org/wiki/Interop%C3%A9rabilit%C3%A9),
compatibles avec la plupart des SIG. On peut citer par exemple le format
[GeoJSON](https://fr.wikipedia.org/wiki/GeoJSON).

- bonne compatibilité avec les données publiques qui se reposent en général
aussi sur ce type de solutions.

Inconvénients, faiblesses:
--------------------------

- Fonctionnalités: Les solutions commerciales ont des fonctionnalités nettement
plus avancées. On peut citer par exemple les fonctionnalités de cartographie 3D
ou de photo (ex: [streetview](https://www.google.com/streetview/).

- Facilité d'utilisation: les API des solutions commerciales sont généralement
très bien documentées avec de nombreux exemples.

- Dimensionnement: les solutions commerciales offrent en général une
infrastructure permettant de soutenir un usage intensif de vos solutions de
cartographie.


# Quelques notions de base en cartographie


Une position sur une carte est en général représentée par un couple: latitude et
longitude.
La latitude et la longitude sont exprimée en float.
Par exemple la position du CERN est:

`46.234120, 6.052646`

`46.234120` représente la latitude et `6.052646` représente la longitude.

La figure suivante illustre la décomposition d'un point (en orange) en un couple
latitude et longitude.

![latitude et longitude](img/lat-long-640x640.jpg)

La latitude zéro est fixée au niveau de l'équateur.
La longitude zéro est fixée au méridien de Greenwich.


# Affichage d'une carte centrée sur l'utilisteur

Pour se familiariser avec OpenLayers, nous allons tout d'abord afficher une
simple carte sur une page html. Puis nous ajouterons une fonctionnalité
permettant de centrer automatiquement la carte sur la zone géographique supposée
de l'utilisateur. Pour cela, nous nous reposerons sur la position déduite de
l'adresse IP de l'utilisateur.

## Affichage d'une carte dans un simple page web

Consultez les explications de la page suivante:
[https://openlayers.org/en/latest/doc/quickstart.html](https://openlayers.org/en/latest/doc/quickstart.html)

Puis créez vous une page web en local sur votre ordinateur affichant une simple
carte.


